# Mirum - Complete PHP Starter App

## Installation

**Mirum** utilizes [Composer](https://getcomposer.org/) and [Bower](http://bower.io/) to manage its dependencies. So, before using **Mirum**, make sure you have Composer and Bower installed on your machine.

1. Clone this repo
2. Move to the cloned folder
3. Checkout the latest stable branch (current: 1.0): `git checkout 1.0`
4. `composer install`
5. Move to `public` folder
6. `bower install`
7. Back to parent folder
8. `php artisan serve`
9. Access `http://localhost:8000` in your browser
10. Follow the installation wizard.
11. Done!