<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Sentinel;

class Category extends Model
{
  use Sluggable;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }

  protected $table = 'categories';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'slug',
  ];

  public static function saveData($param) {
      $userCreated = Sentinel::getUser();
      $data = new Category;
      $data->name = $param['name'];
      $data->created_by = $userCreated->id;

      if ($data->save()) {
          return $data;
      } else {
          return false;
      }
  }

  public static function updateData($param) {
      $data = Category::find($param['id']);

      $data->name = $param['name'];
      if ($data->save()) {
          return $data;
      } else {
          return false;
      }
  }

  public static function getDataCategory($param) {
      $userCreated = Sentinel::getUser();
      $getData = Category::find($param['id']);
      if ($getData) {
        $data['error'] = 0;
        $data['id'] = $getData->id;
        $data['name'] = $getData->name;
        echo json_encode($data);
      } else {
          return false;
      }
  }

  public static function deleteCategory($param) {
      $data = Category::find($param['id']);

      if ($data->delete()) {
          return $data;
      } else {
          return false;
      }
  }
}
