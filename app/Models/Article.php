<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Yajra\Datatables\Datatables;
use Sentinel;


class Article extends Model
{
  use Sluggable;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'title'
          ]
      ];
  }

  protected $table = 'articles';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'category_id',
    'title',
    'short_description',
    'content',
    'slug',
    'image',
    'thumbnail',
  ];

  public static function datatables(){
    return Datatables::of(Article::all())->make(true);
  }

  public static function getTime($timestamp)
      {
          $selisih = time() - strtotime($timestamp) ;

          $detik = $selisih ;
          $menit = round($selisih / 60 );
          $jam = round($selisih / 3600 );
          $hari = round($selisih / 86400 );
          $minggu = round($selisih / 604800 );
          $bulan = round($selisih / 2419200 );
          $tahun = round($selisih / 29030400 );

          if ($detik <= 60) {
              $waktu = $detik.' detik yang lalu';
          } else if ($menit <= 60) {
              $waktu = $menit.' menit yang lalu';
          } else if ($jam <= 24) {
              $waktu = $jam.' jam yang lalu';
          } else if ($hari <= 7) {
              $waktu = $hari.' hari yang lalu';
          } else if ($minggu <= 4) {
              $waktu = $minggu.' minggu yang lalu';
          } else if ($bulan <= 12) {
              $waktu = $bulan.' bulan yang lalu';
          } else {
              $waktu = $tahun.' tahun yang lalu';
          }

          return $waktu;
      }

      public static function saveData($param) {
          $userCreated = Sentinel::getUser();
          $uploadImage = upload_file($param['image'], 'uploads/article/');
          $data = new Article;
          $data->category_id = $param['category'];
          $data->title = $param['title'];
          $data->short_description = $param['short_description'];
          $data->content = $param['content'];
          $data->image = $uploadImage['original'];
          $data->thumbnail = $uploadImage['thumbnail'];
          $data->created_by = $userCreated->id;

          if ($data->save()) {
              return $data;
          } else {
              return false;
          }
      }

      public static function updateData($param) {
          $userCreated = Sentinel::getUser();
          $data = Article::find($param['id']);
          $data->category_id = $param['category'];
          $data->title = $param['title'];
          $data->short_description = $param['short_description'];
          $data->content = $param['content'];

          if ($data->save()) {
              return $data;
          } else {
              return false;
          }
      }

      public static function deleteArticle($param) {
          $data = Article::find($param['id']);

          if ($data->delete()) {
              return $data;
          } else {
              return false;
          }
      }

}
