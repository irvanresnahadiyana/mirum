<?php

namespace App\Http\Middleware;

use Sentinel;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      if (!Sentinel::check() || !Sentinel::inRole('administrator')) {
          session()->flash('status', trans("auth.logged"));

          return redirect()->back();
      }

      return $next($request);
    }
}
