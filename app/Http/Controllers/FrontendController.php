<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;

class FrontendController extends Controller
{
  public function index()
  {
      $data['data'] = Article::join('categories','categories.id','=','articles.category_id')->select('categories.name as categoryName','articles.*')->get();
      return view('contents.frontend.index')->with($data);
  }

  public function detail($slug)
  {
      $data['data'] = Article::join('categories','categories.id','=','articles.category_id')->where('articles.slug',$slug)->select('categories.name as categoryName','articles.*')->first();
      return view('contents.frontend.detail')->with($data);
  }
}
