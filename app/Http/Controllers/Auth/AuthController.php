<?php

namespace App\Http\Controllers\Auth;

use Sentinel;
use Activation;
use Mail;
use URL;
use Socialite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class AuthController extends Controller
{
    /**
     * [login description]
     * @return [type] [description]
     */
    public function login()
    {
        return view('contents.admin.auth.login');
    }

    /**
     * [loginPost description]
     * @param  LoginRequest $request [description]
     * @return [type]                [description]
     */
    public function loginPost(LoginRequest $request)
    {
        $find_user = Sentinel::findByCredentials($request->only('email'));
        if (! $find_user) {
            return response()->json([
                'result' => 'Error',
                'message' => trans('passwords.user'),
            ], 400);
        }

        try {
            $remember = (bool)$request->remember;
            if (! $user = Sentinel::authenticate($request->all(), $remember)) {
                return response()->json([
                    'result' => 'Error',
                    'message' => trans('auth.failed'),
                ], 400);
            }

            session()->flash('status', 'Login success!');

            if ($user->inRole('administrator')) {
              $data['redirect'] = route("admin-dashboard");
            } else {
              $data['redirect'] = route("user-dashboard");
            }

          return json_encode($data);
        } catch (ThrottlingException $e) {
            return response()->json([
                'result' => 'Error',
                'message' => $e->getMessage(),
            ], 400);
        } catch (NotActivatedException $e) {
            return response()->json([
                'result' => 'Error',
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * [socialiteRedirect description]
     * @param  [type] $provider [description]
     * @return [type]           [description]
     */
    public function socialiteRedirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * [socialiteLogin description]
     * @param  [type] $provider [description]
     * @return [type]           [description]
     */
    public function socialiteLogin($provider)
    {
        $user = Socialite::driver($provider)->user();
        $login = $user->email;

        $find_user = Sentinel::findByCredentials(['login' => $login]);
        if (!$find_user) {
            $credentials = [
                'email' => $user->email,
                'password' => 12345678,
            ];

            $find_user = Sentinel::registerAndActivate($credentials);
        }

        if (Activation::completed($find_user)) {
            Sentinel::login($find_user);

            session()->flash('status', 'Login success!');

            return redirect()->route('admin-dashboard');
        }

        session()->flash('status', trans("auth.not_activated"));

        return redirect()->route('auth-login');
    }

    /**
     * [logout description]
     * @return [type] [description]
     */
    public function logout()
    {
        session()->flash('status', 'Logout success!');
        $user = Sentinel::getUser();
        Sentinel::logout($user,true);

        return redirect('/');
    }

    /**
     * [register description]
     * @return [type] [description]
     */
    public function register()
    {
        return view('contents.admin.auth.register');
    }

    /**
     * [registerPost description]
     * @param  RegisterRequest $request [description]
     * @return [type]                   [description]
     */
    public function registerPost(RegisterRequest $request)
    {
        $data = $request->except('password_confirmation');

        $userRegister = Sentinel::registerAndActivate($data);
        $roleUser = Sentinel::findRoleByName('User');
        $roleUser->users()->attach($userRegister);

        session()->flash('status', trans("auth.activation"));

        return response()->json([
            'result' => 'Success',
            'message' => trans("auth.activation"),
        ], 200);
    }

    /**
     * [activate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function activate(Request $request)
    {
        $user = Sentinel::findByCredentials(['login' => $request->login]);

        if (!$user) {
            session()->flash('status', trans("auth.user_not_registered"));

            return redirect()->route('auth-login');
        }

        if (Activation::completed($user)) {
            session()->flash('status', trans("auth.user_already_activated"));

            return redirect()->route('auth-login');
        }

        Activation::removeExpired();

        if (Activation::complete($user, $request->code)) {
            session()->flash('status', trans("auth.user_has_been_activated"));
        } else {
            $activation = (Activation::exists($user)) ? Activation::exists($user) : Activation::create($user);

            // Send email activation
            Mail::send('emails.general', ['title' => trans("auth.activate"), 'content' => '<a href="'.action('Auth\AuthController@activate', ['login' => $user->email, 'code' => $activation->code]).'">Click here</a> to activate your account.'], function ($message) use($user)
            {
                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                $message->to($user->email);
                $message->subject(trans("auth.activate"));
            });

            session()->flash('status', trans("auth.reactivation"));
        }

        return redirect()->route('auth-login');
    }

}
