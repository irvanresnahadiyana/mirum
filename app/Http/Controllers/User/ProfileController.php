<?php

namespace App\Http\Controllers\User;

use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UpdatePassword;
use App\Models\User;

class ProfileController extends Controller
{
    public function index() {
        return view('contents.user.profile');
    }

    public function updateProfile(UpdateProfileRequest $request) {
        $user = user_info();
        $changeUser = User::find($user->id);
        $changeUser->first_name = $request->first_name;
        $changeUser->last_name = $request->last_name;
        $changeUser->save();

        session()->flash('status', trans("general.update_success"));

        return response()->json([
            'result' => 'Success',
            'message' => trans("general.update_success"),
        ], 200);
    }

    public function updatePassword() {
        return view('contents.user.updatePassword');
    }
    public function updatePasswordSave(UpdatePassword $request) {
        $hasher = Sentinel::getHasher();

        $oldPassword = $request->old_password;
        $password = $request->new_password;
        $passwordConf = $request->password_confirmation;

        $user = Sentinel::getUser();

        if (!$hasher->check($oldPassword, $user->password) || $password != $passwordConf) {
            session()->flash('status', trans("general.update_failed"));

            return response()->json([
                'result' => 'Error',
                'message' => trans("general.update_failed"),
            ], 200);
        }

        Sentinel::update($user, array('password' => $password));

        session()->flash('status', trans("general.update_success"));

        return response()->json([
            'result' => 'Success',
            'message' => trans("general.update_success"),
        ], 200);
    }
}
