<?php

namespace App\Http\Controllers\User;

use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Article;

class DashboardController extends Controller
{
    public function index() {
        $userLogin = Sentinel::getUser();
        $data['total_article'] = Article::all()->count() ;
        $data['your_article'] = Article::where('created_by',$userLogin->id)->count() ;

        return view('contents.user.dashboard')->with($data);
    }

}
