<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Yajra\Datatables\Datatables;
use App\Http\Requests\SaveDataCategory;
use Sentinel;


class CategoryController extends Controller
{

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index() {

        return view('contents.user.category.index');
    }

    public function getDataTable()
    {
        $userLogin = Sentinel::getUser();
        $categories = Category::select(['id','name','created_by'])->where('categories.created_by',$userLogin->id);

        return Datatables::of($categories)
            ->addColumn('action', function ($category) {
                return '<a href="/user/edit-category/'.$category->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                        <a href="javascript:void(0)" onclick="DeleteCategory('.$category->id.')" data-toggle="tooltip"  class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
            ->make(true);

    }

    public function create() {

        return view('contents.user.category.create');
    }

    public function edit($id) {
      $data['data'] = Category::find($id);

      return view('contents.user.category.edit')->with($data);

    }

    public function saveData(SaveDataCategory $request)
    {
      try {

          $categoryStore = $this->category->saveData($request);

          $result['result'] = 'success';
          $result['message'] = 'data has been saved';
          return response()->json($result,200);

      } catch (Exception $e) {
          return response()->json([
              'result' => 'Error',
              'message' => $e->getMessage(),
          ], 400);
      }
    }

    public function updateCategory(SaveDataCategory $request)
    {
      try {

          $categoryUpdate = $this->category->updateData($request);

          $result['result'] = 'success';
          $result['message'] = 'data has been updated';
          return response()->json($result,200);

      } catch (Exception $e) {
          return response()->json([
              'result' => 'Error',
              'message' => $e->getMessage(),
          ], 400);
      }
    }

    public function deleteCategory(Request $request)
    {
      $categoryDelete = $this->category->deleteCategory($request);

      $result['result'] = 'success';
      $result['message'] = 'data has been deleted';
      $result['error'] = 0;
      echo json_encode($result);
      exit();
    }
}
