<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use Yajra\Datatables\Datatables;
use App\Http\Requests\SaveDataArticle;
use Sentinel;


class ArticleController extends Controller
{

  public function __construct(Article $article)
  {
      $this->article = $article;
  }

  public function index() {

      return view('contents.user.article.index');
  }

  public function getDataTable()
  {
      $userLogin = Sentinel::getUser();
      $articles = Article::select(['id','slug','category_id','title','short_description','updated_at','created_by'])->where('articles.created_by',$userLogin->id);

      return Datatables::of($articles)
          ->addColumn('action', function ($article) {
              return '<a href="/user/editArticle/'.$article->slug.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                      <a href="/user/viewArticle/'.$article->slug.'" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                      <a href="javascript:void(0)" onclick="DeleteArticle('.$article->id.')" data-toggle="tooltip"  class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
          })
          ->editColumn('category_id', function ($article) {
              $find = Category::find($article->category_id);
              return $find->name;
          })
          ->editColumn('created_at', function ($article) {
              return Article::getTime($article->updated_at);
          })
          ->make(true);

  }

  public function create() {

      $data['category'] = Category::all();
      return view('contents.user.article.create')->with($data);
  }

  public function view($slug) {
    $data['data'] = Article::where('slug',$slug)->first();
    $data['category'] = Category::where('id',$data['data']->category_id)->first();

    return view('contents.admin.article.view')->with($data);
  }

  public function edit($slug) {
    $data['data'] = Article::where('slug',$slug)->first();
    $data['category'] = Category::where('id',$data['data']->category_id)->first();
    $data['categoryAll'] = Category::all();

    return view('contents.user.article.edit')->with($data);

  }

  public function saveData(SaveDataArticle $request)
  {
    try {

        $articleStore = $this->article->saveData($request);

        $result['result'] = 'success';
        $result['message'] = 'data has been saved';
        return response()->json($result,200);

    } catch (Exception $e) {
        return response()->json([
            'result' => 'Error',
            'message' => $e->getMessage(),
        ], 400);
    }
  }

  public function updateArticle(SaveDataArticle $request)
  {
    try {

        $articleUpdate = $this->article->updateData($request);

        $result['result'] = 'success';
        $result['message'] = 'data has been saved';
        return response()->json($result,200);

    } catch (Exception $e) {
        return response()->json([
            'result' => 'Error',
            'message' => $e->getMessage(),
        ], 400);
    }
  }

  public function deleteArticle(Request $request)
  {
    $articleDelete = $this->article->deleteArticle($request);

    $result['result'] = 'success';
    $result['message'] = 'data has been deleted';
    $result['error'] = 0;
    echo json_encode($result);
    exit();
  }

}
