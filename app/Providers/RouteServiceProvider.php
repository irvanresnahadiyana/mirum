<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $namespace_auth = 'App\Http\Controllers\Auth';
    protected $namespace_admin = 'App\Http\Controllers\Admin';
    protected $namespace_merchant = 'App\Http\Controllers\User';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
        $this->mapAuthRoutes();
        $this->mapAdminRoutes();
        $this->mapMerchantRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    protected function mapAuthRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_auth,
            'prefix' => 'auth',
        ], function ($router) {
            require base_path('routes/auth.php');
        });
    }

    protected function mapAdminRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_admin,
            'prefix' => 'admin',
        ], function ($router) {
            require base_path('routes/admin.php');
        });
    }

    protected function mapMerchantRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_merchant,
            'prefix' => 'user',
        ], function ($router) {
            require base_path('routes/user.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */

}
