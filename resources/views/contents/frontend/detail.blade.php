<!--
  Author: W3layouts
  Author URL: http://w3layouts.com
  License: Creative Commons Attribution 3.0 Unported
  License URL: http://creativecommons.org/licenses/by/3.0/
  -->
<!DOCTYPE HTML>
<html>
  <head>
    <title>Mirum</title>
    {!! Html::style('frontend_assets/css/style.css') !!}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    </script>
    <!----webfonts---->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!----//webfonts---->
    <!---start-click-drop-down-menu----->
    <script src="js/jquery.min.js"></script>
    <!----start-dropdown--->
    <script type="text/javascript">
      var $ = jQuery.noConflict();
      	$(function() {
      		$('#activator').click(function(){
      			$('#box').animate({'top':'0px'},500);
      		});
      		$('#boxclose').click(function(){
      		$('#box').animate({'top':'-700px'},500);
      		});
      	});
      	$(document).ready(function(){
      	//Hide (Collapse) the toggle containers on load
      	$(".toggle_container").hide();
      	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
      	$(".trigger").click(function(){
      		$(this).toggleClass("active").next().slideToggle("slow");
      			return false; //Prevent the browser jump to the link anchor
      	});

      });
    </script>
    <!----//End-dropdown--->
  </head>
  <body>
    <!---start-wrap---->
    <!---start-header---->
    <div class="header">
      <div class="wrap">
        <a href="{{ URL::to('/') }}"><div class="logo" style="margin-top:12px">
          MIRUM
        </div></a>
      </div>
      <!-- <div class="top-searchbar">
        <form>
        	<input type="text" /><input type="submit" value="" />
        </form>
        </div> -->
      <div class="userinfo">
        <div class="user">
          <ul>
            <li><a href="#"><img src="images/user-pic.png" title="user-name" /><span>USERNAME</span></a></li>
          </ul>
        </div>
      </div>
      <div class="clear"> </div>
    </div>
    </div>
    <!---//End-header---->
    <!---start-content---->
    <div class="content">
      <div class="wrap">
        <div class="single-page">
          <div class="single-page-artical">
            <div class="artical-content">
              <img src="{{ URL::to('/') }}/{{ $data->image }}">
							<h6 style="padding-top:20px; font-style:italic">Category : {{ $data->categoryName }}</h6>
              <h3><a href="#">{!! $data->title !!}</a></h3>
              <p>{!! $data->content !!}</p>
            </div>
            <!-- <div class="artical-links">
              <ul>
                <li><a href="#"><img src="images/blog-icon2.png" title="Admin"><span>admin</span></a></li>
                <li><a href="#"><img src="images/blog-icon3.png" title="Comments"><span>No comments</span></a></li>
                <li><a href="#"><img src="images/blog-icon4.png" title="Lables"><span>View posts</span></a></li>
              </ul>
            </div> -->
            <!-- <div class="share-artical">
              <ul>
                <li><a href="#"><img src="images/facebooks.png" title="facebook">Facebook</a></li>
                <li><a href="#"><img src="images/twiter.png" title="Twitter">Twiiter</a></li>
                <li><a href="#"><img src="images/google+.png" title="google+">Google+</a></li>
                <li><a href="#"><img src="images/rss.png" title="rss">Rss</a></li>
              </ul>
            </div> -->
            <div class="clear"> </div>
          </div>
        </div>
      </div>
    </div>
    <!----start-footer--->
    <div class="footer">
      <p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
    </div>
    <!----//End-footer--->
    <!---//End-wrap---->
  </body>
</html>
