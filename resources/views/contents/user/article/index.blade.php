@extends('layouts/admin/master/admin_template')

@section('title', 'Article')
@section('page_title', 'Article')
@section('page_description', 'Manage Article')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('user-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
	<div class='row'>
    <section class="content">
      <div class="col-md-12">
        <div class="button-head">
          <a href="{{route('user-create-article')}}"><button class="btn btn-flat btn-primary"><i class="fa fa-plus"></i> Add New Article</button></a>
        </div>

        <div id="dataTable">
          <div class="box">
              <div class="box-body table-responsive">
                  <table id="article-table" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                            <th>Category</th>
                            <th>Title</th>
                            <th>Short Description</th>
                            <th width="180px">Action</th>
                        </tr>
                      </thead>
                  </table>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
      </div>
  	</section>
  </div><!-- /.row -->
@endsection
{!! Html::script('js/jquery.js') !!}
<script>
$(function() {
    $('#article-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{route('user-article-datatable')}}",
        columns: [
            { data: 'category_id', name: 'category_id' },
            { data: 'title', name: 'title' },
            { data: 'short_description', name: 'short_description' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

});

function DeleteArticle(id) {
    $('#ConfirmDelete').modal('show');
    $('#yes-delete').click(function() {
        $.ajax({
            type: 'POST',
            url: "{{route('user-delete-article')}}",
            data: 'id=' + id,
            dataType: 'json',
            success: function(data) {
                if (data.error === 0) {
                    $('#ConfirmDelete').modal('hide');
                    location.reload();
                } else {
                    $('#InformModal').modal('show');
                    $('#InformModal #message').html(data.message);
                    $('#ConfirmDelete').modal('hide');
                }


            }
        });

    });
}
</script>
