@extends('layouts/admin/master/admin_template')

@section('title', 'Profile')
@section('page_title', 'Profile')
@section('page_description', 'Setting')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="#"><i class="fa fa-user"></i> {{ trans('general.profile') }}</a></li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-widget widget-user">
                <div class="widget-user-header bg-aqua-active">
                    <h3 class="widget-user-username">{{ user_info('full_name') }}</h3>
                    <h5 class="widget-user-desc">{{ user_info('role')->name }}</h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="{{ link_to_avatar() }}" alt="User Avatar">
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <form role="form" id="form-profile">
                                <div class="form-group" id="update-email">
                                    <label for="email">{{ trans('general.email_address') }}</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="{{ user_info('email') }}" readonly="">
                                </div>
                                <div class="form-group" id="update-first_name">
                                    <label for="first_name">{{ trans('general.first_name') }}</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter first name" value="{{ user_info('first_name') }}">
                                </div>
                                <div class="form-group" id="update-last_name">
                                    <label for="last_name">{{ trans('general.last_name') }}</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter last name" value="{{ user_info('last_name') }}">
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary" id="update-profile">{{ trans('general.button.save') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="widget-user-header bg-aqua-active">
                    <h3 class="widget-user-username">Want to change your password ?</h3>
                    <a href="{{ route('user-update-password')}}"
                      <button class="btn btn-danger" id="update-password">Update Password</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#update-profile').click(function(event) {
            event.preventDefault();
            var $btn = $(this).button('loading');
            $('.form-group').removeClass('has-error');
            $('.help-block').remove();
            $.ajax({
                type:'post',
                url:'{{ route("user-update-profile") }}',
                data: new FormData($("#form-profile")[0]),
                dataType: 'json',
                async: false,
                processData: false,
                contentType: false,
                success:function(data) {
                    window.location = '{{ route("profile") }}';
                },
                error:function(data) {
                    var data = data.responseJSON;
                    $.each(data,function(key, val){
                        $('#update-'+key).addClass('has-error');
                        $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
                    });
                    $btn.button('reset');
                }
            });
        });
    </script>

@endsection
