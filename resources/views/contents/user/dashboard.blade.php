@extends('layouts/admin/master/admin_template')

@section('title', 'Dashboard')
@section('page_title', 'Dashboard')
@section('page_description', 'User Dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('user-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
  <!-- Info boxes -->
  <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $total_article }}</h3>

          <p>Total Article</p>
        </div>
        <div class="icon">
          <i class="fa fa-newspaper-o"></i>
        </div>
      </div>
    </div>
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $your_article }}</h3>

          <p>Your Article</p>
        </div>
        <div class="icon">
          <i class="fa fa-newspaper-o"></i>
        </div>
      </div>
    </div>
    <!-- /.col -->
  </div>

@endsection
