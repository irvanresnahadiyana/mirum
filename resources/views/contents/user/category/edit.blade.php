@extends('layouts/admin/master/admin_template')

@section('title', 'Category')
@section('page_title', 'Category')
@section('page_description', 'Update Category')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('user-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
	<div class='row'>
    <section class="content">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <form id="form-category" class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8">
                <div class="box-body">
                    <div class="error"></div>
                    <div class="form-group" id="save-category">
                      <label class="col-sm-2 control-label">Category Name</label>
                        <div class="col-sm-9">
                          <input type="text" name="name" value="{{ $data->name }}" class="form-control">
                          <input type="hidden" name="id" value="{{ $data->id }}"class="form-control">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{ route('admin-category') }}" class="btn btn-default">Cancel</a>
                    <button class="btn btn-primary pull-right" id="submit-save" type="submit">Save</button>
                </div>
            </form>
        </div>
      </div>
  	</section>
  </div><!-- /.row -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
@endsection
@section('script')
<script type="text/javascript">

  $(function() {
      $(".textarea").wysihtml5();

  });


  $('#form-category').submit(function(event) {
      event.preventDefault();
      var $btn = $('#submit-save').button('loading');
      $('.form-group').removeClass('has-error');
      $('.help-block').remove();
      var formData = new FormData(this);
      $.ajax({
          type:'post',
          url:'{{ route("user-update-category") }}',
          data: formData,
          cache:false,
          contentType: false,
          processData: false,
          dataType: 'json',
          success:function(data) {
              window.location = '{{ route("user-category") }}';
              generateNotif('topCenter', 'error', '<i class="fa fa-success" aria-hidden="true"></i> '+data.message);
              $btn.button('reset');
          },
          error:function(data) {
              var data = data.responseJSON;

              if (typeof data.result == 'Error') {
                  generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> '+data.message);
                  $('.form-group').addClass('has-error');
              } else {
                  $.each(data,function(key, val){
                      $('#category-'+key).addClass('has-error');
                      $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
                  });
              }

              $btn.button('reset');
          }
      });
  });
  </script>
@endsection
