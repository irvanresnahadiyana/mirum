@extends('layouts/admin/master/admin_template')

@section('title', 'Category')
@section('page_title', 'Category')
@section('page_description', 'Manage Category')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('user-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
	<div class='row'>
    <section class="content">
      <div class="col-md-12">
        <div class="button-head">
          <a href="{{route('user-create-category')}}"><button class="btn btn-flat btn-primary"><i class="fa fa-plus"></i> Add New Category</button></a>
        </div>

        <div id="dataTable">
          <div class="box">
              <div class="box-body table-responsive">
                  <table id="category-table" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                            <th>Category</th>
                            <th width="180px">Action</th>
                        </tr>
                      </thead>
                  </table>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
      </div>
  	</section>
  </div><!-- /.row -->
@endsection
{!! Html::script('js/jquery.js') !!}
<script>
$(function() {
    $('#category-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{route('user-category-datatable')}}",
        columns: [
            { data: 'name', name: 'name' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

});

function DeleteCategory(id) {
    $('#ConfirmDelete').modal('show');
    $('#yes-delete').click(function() {
        $.ajax({
            type: 'POST',
            url: "{{route('user-delete-category')}}",
            data: 'id=' + id,
            dataType: 'json',
            success: function(data) {
                if (data.error === 0) {
                    $('#ConfirmDelete').modal('hide');
                    location.reload();
                } else {
                    $('#InformModal').modal('show');
                    $('#InformModal #message').html(data.message);
                    $('#ConfirmDelete').modal('hide');
                }


            }
        });

    });
}
</script>
