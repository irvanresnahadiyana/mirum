@extends('layouts/admin/master/admin_template')

@section('title', 'Category')
@section('page_title', 'Category')
@section('page_description', 'Manage Category')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
	<div class='row'>
    <section class="content">
      <div class="col-md-12">
        <div class="button-head">
          <a href="{{route('admin-create-category')}}"><button class="btn btn-flat btn-primary"><i class="fa fa-plus"></i> Add New Category</button></a>
        </div>

        <div id="dataTable">
          <div class="box">
              <div class="box-body table-responsive">
                  <table id="category-table" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                            <th>Category</th>
                            <th width="180px">Action</th>
                        </tr>
                      </thead>
                  </table>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
      </div>
  	</section>
  </div><!-- /.row -->
  <div class="modal fade" id="CategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel"><i class="fa fa-check-square"></i> Form</h4>
          </div>
          <div class="modal-body">
              <div class="box-body row">
                <form id="form-update-category" class="form-horizontal">
                    <div class="box-body">
                        <div class="error"></div>
                        <div class="form-group" id="save-category">
                          <label class="col-sm-3 control-label">Category Name</label>
                            <div class="col-sm-9">
                              <input type="text" name="name" id="name" class="form-control">
                              <input type="hidden" name="id" id="id" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-danger" data-dismiss="modal"></i> Cancel</button>
                        <button class="btn btn-primary pull-right" id="submit-save" type="submit">Save</button>
                    </div>
                </form>
              </div>
          </div><!-- /.tab-pane -->
        </div>
        <div class="modal-footer">
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
{!! Html::script('js/jquery.js') !!}
<script>
$(function() {
    $('#category-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{route('admin-category-datatable')}}",
        columns: [
            { data: 'name', name: 'name' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

});


$('#form-update-category').submit(function(event) {
    event.preventDefault();
    var $btn = $('#submit-save').button('loading');
    $('.form-group').removeClass('has-error');
    $('.help-block').remove();
    var formData = new FormData(this);
    $.ajax({
        type:'post',
        url:'{{ route("admin-update-category") }}',
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success:function(data) {
            window.location = '{{ route("admin-category") }}';
            generateNotif('topCenter', 'error', '<i class="fa fa-success" aria-hidden="true"></i> '+data.message);
            $btn.button('reset');
        },
        error:function(data) {
            var data = data.responseJSON;

            if (typeof data.result == 'Error') {
                generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> '+data.message);
                $('.form-group').addClass('has-error');
            } else {
                $.each(data,function(key, val){
                    $('#category-'+key).addClass('has-error');
                    $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
                });
            }

            $btn.button('reset');
        }
    });
});

function DeleteCategory(id) {
    $('#ConfirmDelete').modal('show');
    $('#yes-delete').click(function() {
        $.ajax({
            type: 'POST',
            url: "{{route('admin-delete-category')}}",
            data: 'id=' + id,
            dataType: 'json',
            success: function(data) {
                if (data.error === 0) {
                    $('#ConfirmDelete').modal('hide');
                    location.reload();
                } else {
                    $('#InformModal').modal('show');
                    $('#InformModal #message').html(data.message);
                    $('#ConfirmDelete').modal('hide');
                }


            }
        });

    });
}
</script>
