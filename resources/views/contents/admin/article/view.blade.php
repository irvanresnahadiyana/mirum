@extends('layouts/admin/master/admin_template')

@section('title', 'Article')
@section('page_title', 'Article')
@section('page_description', 'Detail Article')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
	<div class='row'>
    <section class="content">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <div class="content">
              <div class="wrap">
                <div class="single-page">
                  <div class="single-page-artical">
                    <div class="artical-content">
                      <img src="{{ URL::to('/') }}/{{ $data->image }}">
        							<h6 style="padding-top:20px; font-style:italic">Category : {{ $category->name }}</h6>
                      <h3><a href="#">{!! $data->title !!}</a></h3>
                      <p>{!! $data->content !!}</p>
                    </div>
                    <!-- <div class="artical-links">
                      <ul>
                        <li><a href="#"><img src="images/blog-icon2.png" title="Admin"><span>admin</span></a></li>
                        <li><a href="#"><img src="images/blog-icon3.png" title="Comments"><span>No comments</span></a></li>
                        <li><a href="#"><img src="images/blog-icon4.png" title="Lables"><span>View posts</span></a></li>
                      </ul>
                    </div> -->
                    <!-- <div class="share-artical">
                      <ul>
                        <li><a href="#"><img src="images/facebooks.png" title="facebook">Facebook</a></li>
                        <li><a href="#"><img src="images/twiter.png" title="Twitter">Twiiter</a></li>
                        <li><a href="#"><img src="images/google+.png" title="google+">Google+</a></li>
                        <li><a href="#"><img src="images/rss.png" title="rss">Rss</a></li>
                      </ul>
                    </div> -->
                    <div class="clear"> </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
  	</section>
  </div><!-- /.row -->
@endsection
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
@section('script')
@endsection
