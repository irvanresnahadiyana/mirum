@extends('layouts/admin/master/admin_template')

@section('title', 'Article')
@section('page_title', 'Article')
@section('page_description', 'Create Article')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
	<div class='row'>
    <section class="content">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <form id="form-article" class="form-horizontal" enctype="multipart/form-data" >
                <div class="box-body">
                    <div class="error"></div>
                    <div class="form-group" id="article-category">
                        <label class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-9">
                          <select class="form-control select2" placeholder="Category" name="category" id="parent_category">
                            <option></option>
                              @foreach($category as $val)
                              <option name="category" value="{{ $val->id }}" >{{ $val->name }}</option>
                              @endforeach
                          </select>
                        </div>
                    </div>
                    <div class="form-group" id="article-title">
                      <label class="col-sm-2 control-label">Article Name</label>
                        <div class="col-sm-9">
                          <input type="text" name="title" class="form-control" id="title">
                        </div>
                    </div>
                    <div class="form-group" id="article-image">
                      <label class="col-sm-2 control-label">Select Image</label>
                      <div class="col-sm-9">
                          <input type="file" name="image" class="form-control" id="image">
                      </div>
                    </div>
                    <div class="box" id="article-short_description">
                      <div class="box-header">
                        <h3 class="box-title"><b>Short Description :</b></h3>
                      </div>
                      <div class="box-body pad">
                        <div class="form-group" id="form-short_description">
                          <textarea class="textarea" name="short_description" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="box">
                      <div class="box-header">
                        <h3 class="box-title"><b>Content Description :</b></h3>
                      </div>
                      <div class="box-body pad">
                        <div class="form-group" id="form-content">
                          <textarea class="textarea" name="content" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{ route('admin-article') }}" class="btn btn-default">Cancel</a>
                    <button class="btn btn-primary pull-right" id="submit-save" type="submit">Save</button>
                </div>
            </form>
        </div>
      </div>
  	</section>
  </div><!-- /.row -->
@endsection
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
@section('script')
<script type="text/javascript">

$(function() {
    $(".textarea").wysihtml5();

    $('#article-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{route('admin-article-datatable')}}",
        columns: [
            { data: 'category_id', name: 'category_id' },
            { data: 'title', name: 'title' },
            { data: 'short_description', name: 'short_description' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

});

$('#form-article').submit(function(event) {
    event.preventDefault();
    var $btn = $('#submit-save').button('loading');
    $('.form-group').removeClass('has-error');
    $('.help-block').remove();
    var formData = new FormData(this);
    $.ajax({
        type:'post',
        url:'{{ route("admin-save-article") }}',
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success:function(data) {
            window.location = '{{ route("admin-article") }}';
            generateNotif('topCenter', 'error', '<i class="fa fa-success" aria-hidden="true"></i> '+data.message);
            $btn.button('reset');
        },
        error:function(data) {
            var data = data.responseJSON;

            if (typeof data.result == 'Error') {
                generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> '+data.message);
                $('.form-group').addClass('has-error');
            } else {
                $.each(data,function(key, val){
                    $('#article-'+key).addClass('has-error');
                    $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
                });
            }

            $btn.button('reset');
        }
    });
});
</script>
@endsection
