@extends('layouts/admin/master/admin_auth_template')

@section('title', 'Register User')

@section('content')
    <div class="register-box">
        <div class="register-logo">
            <a href="{{ route('admin-dashboard') }}"><b>Mirum</b></a>
        </div>
        <div class="register-box-body">
            <p class="login-box-msg">Register a new membership</p>
            <form id="form-register">
                <div class="form-group has-feedback" id="register-email">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback" id="register-password">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback" id="register-password_confirmation">
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Password Confirmation">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">

                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" id="button-register">Register</button>
                    </div>
                </div>
            </form>
            <div class="social-auth-links text-center">
                <!-- <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-twitter btn-flat"><i class="fa fa-twitter"></i> Sign in using Twitter</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a> -->
            </div>
            <a href="{{ route('auth-login') }}" class="text-center">I already have a membership</a>
        </div>
    </div>
@endsection
