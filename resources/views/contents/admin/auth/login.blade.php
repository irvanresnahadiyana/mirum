@extends('layouts/admin/master/admin_auth_template')

@section('title', 'Admin Login')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ route('admin-dashboard') }}"><b>Mirum</b>Test</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form id="form-login">
                <div class="form-group has-feedback" id="login-email">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback" id="login-password">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" id="button-login" data-loading-text="Loading..." autocomplete="off">Sign In</button>
                    </div>
                </div>
            </form>
            <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="{{ route('auth-redirect-socialite', 'facebook') }}" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
                <!-- <a href="{{ route('auth-login-socialite', 'twitter') }}" class="btn btn-block btn-social btn-twitter btn-flat"><i class="fa fa-twitter"></i> Sign in using Twitter</a>
                <a href="{{ route('auth-login-socialite', 'google') }}" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a> -->
            </div>
            <!-- <a href="#">I forgot my password</a><br> -->
            <a href="{{ route('auth-register') }}" class="text-center"><button type="submit" class="btn btn-danger btn-block btn-flat">Register</button></a>
        </div>
    </div>
@endsection
