@extends('layouts/admin/master/admin_template')

@section('title', 'Profile')
@section('page_title', 'Profile')
@section('page_description', 'Setting')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('general.dashboard') }}</a></li>
        <li><a href="#"><i class="fa fa-user"></i> {{ trans('general.profile') }}</a></li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-widget widget-user">
                <div class="widget-user-header bg-aqua-active">
                    <h3 class="widget-user-username">{{ user_info('full_name') }}</h3>
                    <h5 class="widget-user-desc">{{ user_info('role')->name }}</h5>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <form role="form" id="form-profile">
                                <div class="form-group" id="update-old_password">
                                    <label for="old_password">Old Password</label>
                                    <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Enter current password">
                                </div>
                                <div class="form-group" id="update-new_password">
                                  <label for="new_password">New Password</label>
                                  <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Enter new password">
                                </div>
                                <div class="form-group" id="update-password_confirmation">
                                  <label for="password_confirmation">Password Confirmation</label>
                                  <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Retype new password">
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary" id="update-password">{{ trans('general.button.save') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#update-password').click(function(event) {
            event.preventDefault();
            var $btn = $(this).button('loading');
            $('.form-group').removeClass('has-error');
            $('.help-block').remove();
            $.ajax({
                type:'post',
                url:'{{ route("admin-save-password") }}',
                data: new FormData($("#form-profile")[0]),
                dataType: 'json',
                async: false,
                processData: false,
                contentType: false,
                success:function(data) {
                    window.location = '{{ route("admin-profile") }}';
                },
                error:function(data) {
                    var data = data.responseJSON;
                    $.each(data,function(key, val){
                        $('#update-'+key).addClass('has-error');
                        $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
                    });
                    $btn.button('reset');
                }
            });
        });
    </script>

@endsection
