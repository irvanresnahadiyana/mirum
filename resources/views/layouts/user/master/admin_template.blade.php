<!DOCTYPE html>
<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
        {!! Html::meta('csrf-token', csrf_token()) !!}
        <title>{{ env('APP_NAME', 'MyCms Web Admin') }} | @yield('title', 'Admin')</title>
        <!-- Tell the browser to be responsive to screen width -->
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}
        <!-- Bootstrap 3.3.6 -->
        {!! Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        <!-- Font Awesome -->
        {!! Html::style('bower_components/font-awesome/css/font-awesome.min.css') !!}
        <!-- Theme style -->
        {!! Html::style('bower_components/admin-lte/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('bower_components/admin-lte/dist/css/skins/_all-skins.min.css') !!}
        <!-- Pace style -->
        {!! Html::style('bower_components/admin-lte/plugins/pace/pace.min.css') !!}
        <!-- Animate style Noty-->
        {!! Html::style('bower_components/noty/demo/animate.css') !!}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
-->
    <body class="hold-transition skin-purple sidebar-mini">
        <div class="wrapper">
            <!-- Header -->
            @include('layouts/admin/partials/header')

            <!-- Sidebar -->
            @include('layouts/admin/partials/sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        @yield('page_title', 'Admin')
                        <small>@yield('page_description', 'Admin')</small>
                    </h1>
                    @yield('breadcrumb')
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Your Page Content Here -->
                    @yield('content')
                </section>
                <div class="modal fade" id="ConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-question-circle"></i> Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            Are you sure, delete this data?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-flat btn-danger btn-yes" id="yes-delete">Yes</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Footer -->
            @include('layouts/admin/partials/footer')
        </div>
        <!-- ./wrapper -->
        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery-->
        {!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}
        
        <!-- Bootstrap-->
        {!! Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        <!-- AdminLTE App -->
        {!! Html::script('bower_components/admin-lte/dist/js/app.min.js') !!}
        <!-- PACE -->
        {!! Html::script('bower_components/admin-lte/plugins/pace/pace.min.js') !!}
        <!-- Noty -->
        {!! Html::script('bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') !!}
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).ajaxStart(function() {
                Pace.restart();
            });

            $('.ajax').click(function(){
                $.ajax({url: '#', success: function(result){
                    $('.ajax-content').html('<hr>Ajax Request Completed !');
                }});
            });

            var status = "{{ session('status') }}";
            if (status) {
                generateNotif('topRight', 'success', status);
            }

            function generateNotif(layout, type, text = '') {
                $.noty.closeAll();
                notif = noty({
                    layout      : layout,
                    theme       : 'relax',
                    type        : type,
                    text        : text,
                    dismissQueue: true,
                    animation   : {
                        open  : 'animated bounceInRight',
                        close : 'animated bounceOutRight',
                        easing: 'swing',
                        speed : 500
                    },
                    timeout: 5000,
                    modal: false,
                    maxVisible: 1,
                    closeWith: ['click'],
                });
            }
        </script>
    </body>
</html>
