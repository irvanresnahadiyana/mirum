<!DOCTYPE html>
<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
        {!! Html::meta('csrf-token', csrf_token()) !!}
        <title>{{ env('APP_NAME', 'MyCms Web Admin') }} | @yield('title', 'Admin Login')</title>
        <!-- Tell the browser to be responsive to screen width -->
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}
        <!-- Bootstrap 3.3.6 -->
        {!! Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        <!-- Font Awesome -->
        {!! Html::style('bower_components/font-awesome/css/font-awesome.min.css') !!}
        <!-- Theme style -->
        {!! Html::style('bower_components/admin-lte/dist/css/AdminLTE.min.css') !!}
        <!-- iCheck -->
        {!! Html::style('bower_components/admin-lte/plugins/iCheck/square/blue.css') !!}
        <!-- Pace style -->
        {!! Html::style('bower_components/admin-lte/plugins/pace/pace.min.css') !!}
        <!-- Animate style Noty-->
        {!! Html::style('bower_components/noty/demo/animate.css') !!}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <!-- Your Page Content Here -->
        @yield('content')
        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery-->
        {!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}
        <!-- Bootstrap-->
        {!! Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        <!-- iCheck -->
        {!! Html::script('bower_components/admin-lte/plugins/iCheck/icheck.min.js') !!}
        <!-- PACE -->
        {!! Html::script('bower_components/admin-lte/plugins/pace/pace.min.js') !!}
        <!-- Noty -->
        {!! Html::script('bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') !!}
        {!! Html::script('bower_components/noty/js/noty/themes/bootstrap.js') !!}
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).ajaxStart(function() {
                Pace.restart();
            });

            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });

            var status = "{{ session('status') }}";
            if (status) {
                generateNotif('topCenter', 'alert', status);
            }

            function generateNotif(layout, type, text = '') {
                $.noty.closeAll();
                notif = noty({
                    layout : layout,
                    theme : 'bootstrapTheme',
                    type : type,
                    text : text,
                    dismissQueue: true,
                    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
                    animation : {
                        open : 'animated bounceInDown',
                        close : 'animated bounceOutUp',
                        easing : 'swing',
                        speed : 500
                    },
                    timeout: 10000,
                    maxVisible: 1,
                    closeWith: ['click'],
                });
                // console.log('html: ' + notif.options.id);
            }

            // $(document).ready(function () {
            //     generateAll();
            // });

            // function generateAll() {
            //     generateNotif('top', 'alert');
            //     generateNotif('topCenter', 'information');
            //     generateNotif('topLeft', 'error');
            //     generateNotif('topRight', 'warning');
            //     generateNotif('center', 'notification');
            //     generateNotif('centerLeft', 'success');
            //     generateNotif('centerRight', 'success');
            //     generateNotif('bottom', 'success');
            //     generateNotif('bottomCenter', 'success');
            //     generateNotif('bottomLeft', 'success');
            //     generateNotif('bottomRight', 'success');
            // }

            $('#button-login').click(function(event) {
                event.preventDefault();
                var $btn = $(this).button('loading');
                $('.form-group').removeClass('has-error');
                $('.help-block').remove();
                $.ajax({
                    type:'post',
                    url:'{{ route("auth-login-post") }}',
                    data: $('#form-login').serialize(),
                    dataType: 'json',
                    success:function(data) {
                        window.location = data.redirect;
                    },
                    error:function(data) {
                        var data = data.responseJSON;
                        if (data.result) {
                            generateNotif('topCenter', 'error', '<i class="fa fa-warning" aria-hidden="true"></i> '+data.message);
                            $('.form-group').addClass('has-error');
                        }

                        $.each(data,function(key, val){
                            $('#login-'+key).addClass('has-error');
                            $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
                        });
                        $btn.button('reset');
                    }
                });
            });

            $('#button-register').click(function(event) {
                event.preventDefault();
                var $btn = $(this).button('loading');
                $('.form-group').removeClass('has-error');
                $('.help-block').remove();
                $.ajax({
                    type:'post',
                    url:'{{ route("auth-register-post") }}',
                    data: $('#form-register').serialize(),
                    dataType: 'json',
                    success:function(data) {
                        generateNotif('topCenter', 'success', '{{ trans("auth.activation") }}');
                        window.location = '{{ route("auth-login") }}';
                    },
                    error:function(data) {
                        var data = data.responseJSON;
                        if (data.result) {
                            generateNotif('topCenter', 'error', data.message);
                            $('.form-group').addClass('has-error');
                        }

                        $.each(data,function(key, val){
                            $('#register-'+key).addClass('has-error');
                            $('<span class="help-block">'+val+'</span>').insertAfter($('#'+key));
                        });
                        $btn.button('reset');
                    }
                });
            });
        </script>
    </body>
</html>
