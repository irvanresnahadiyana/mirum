<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ link_to_avatar() }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{user_info('full_name')}}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Main Menu</li>
            <!-- Optionally, you can add icons to the links -->
            @if(user_info()->inRole('administrator'))
                <li class=""><a href="{{route('admin-dashboard')}}"><i class="fa fa-tachometer "></i> <span>Dashboard</span></a></li>
                <li class=""><a href="{{route('admin-category')}}"><i class="fa fa-navicon"></i> <span>Category Article</span></a></li>
                <li class=""><a href="{{route('admin-article')}}"><i class="fa fa-newspaper-o"></i> <span>Article</span></a></li>
            @endif
            @if(user_info()->inRole('user'))
                <li class=""><a href="{{route('user-dashboard')}}"><i class="fa fa-tachometer "></i> <span>Dashboard</span></a></li>
                <li class=""><a href="{{route('user-category')}}"><i class="fa fa-navicon"></i> <span>Category Article</span></a></li>
                <li class=""><a href="{{route('user-article')}}"><i class="fa fa-newspaper-o"></i> <span>Article</span></a></li>
            @endif
            <li class=""><a href="{{ route('auth-logout') }}"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
