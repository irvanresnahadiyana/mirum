<!-- Main Footer -->
<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
		<strong>Mirum</strong>
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
</footer>
