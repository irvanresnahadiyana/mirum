<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <a href="{{ user_info('administrator') ? route('admin-dashboard') : route('user-dashboard') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>M</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Mirum</b>Articles</span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{ link_to_avatar() }}" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ user_info('full_name') }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{ link_to_avatar() }}" class="img-circle" alt="User Image">
                            <p>
                                {{ user_info('full_name') }}
                                <small>Since {{ date('M, Y',strtotime(user_info('created_at'))) }}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    {{ user_info('role')->name }}
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                              @if(user_info()->inRole('user'))
                                <a href="{{ route('profile') }}" class="btn btn-default btn-flat">Profile</a>
                              @endif
                              @if(user_info()->inRole('administrator'))
                                <a href="{{ route('admin-profile') }}" class="btn btn-default btn-flat">Profile</a>
                              @endif
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('auth-logout') }}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
