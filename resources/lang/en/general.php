<?php

return [

    'dashboard' => 'Dashboard',

    'update_success' => 'Update Success',
    'email_address' => 'Email Address',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',

    'button' => [
    	'save' => 'Save',
    ],

];
