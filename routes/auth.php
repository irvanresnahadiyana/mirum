<?php

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Login & Logout
Route::group(['middleware' => 'sentinel_guest'], function () {
	Route::get('login', array('as' => 'auth-login', 'uses' => 'AuthController@login'));
	Route::post('login-post', array('as' => 'auth-login-post', 'uses' => 'AuthController@loginPost'));
});
Route::get('logout', array('as' => 'auth-logout', 'uses' => 'AuthController@logout'));

// Socialite login
Route::get('socialite/redirect/{provider}', array('as' => 'auth-redirect-socialite', 'uses' => 'AuthController@socialiteRedirect'));
Route::get('socialite/login/{provider}', array('as' => 'auth-login-socialite', 'uses' => 'AuthController@socialiteLogin'));

// Register
Route::get('register', array('as' => 'auth-register', 'uses' => 'AuthController@register'));
Route::post('register-post', array('as' => 'auth-register-post', 'uses' => 'AuthController@registerPost'));

// Activation
Route::get('activate', array('as' => 'auth-activate', 'uses' => 'AuthController@activate'));

// Forgot password

// Reset password
