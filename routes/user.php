<?php

/*
|--------------------------------------------------------------------------
| Merchant Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'user_auth'], function () {
	Route::get('dashboard', array('as' => 'user-dashboard', 'uses' => 'DashboardController@index'));
	Route::get('profile', array('as' => 'profile', 'uses' => 'ProfileController@index'));
	Route::post('update-profile', array('as' => 'user-update-profile', 'uses' => 'ProfileController@updateProfile'));
	Route::get('update-password', array('as' => 'user-update-password', 'uses' => 'ProfileController@updatePassword'));
	Route::post('save-password', array('as' => 'user-save-password', 'uses' => 'ProfileController@updatePasswordSave'));

	/* Article */
	Route::get('article', array('as' => 'user-article', 'uses' => 'ArticleController@index'));
	Route::get('datatableArticle', array('as' => 'user-article-datatable', 'uses' => 'ArticleController@getDataTable'));
	Route::get('create-article', array('as' => 'user-create-article', 'uses' => 'ArticleController@create'));
	Route::post('save-article', array('as' => 'user-save-article', 'uses' => 'ArticleController@saveData'));
	Route::post('delete-article', array('as' => 'user-delete-article', 'uses' => 'ArticleController@deleteArticle'));
	Route::post('edit', array('as' => 'user-edit-article', 'uses' => 'ArticleController@edit'));
	Route::get('editArticle/{slug}', array('as' => 'user-getEdit-article', 'uses' => 'ArticleController@edit'));
	Route::post('update-article', array('as' => 'user-update-article', 'uses' => 'ArticleController@updateArticle'));

	/* Category */
	Route::get('category', array('as' => 'user-category', 'uses' => 'CategoryController@index'));
	Route::get('datatableCategory', array('as' => 'user-category-datatable', 'uses' => 'CategoryController@getDataTable'));
	Route::get('create-category', array('as' => 'user-create-category', 'uses' => 'CategoryController@create'));
	Route::post('save-category', array('as' => 'user-save-category', 'uses' => 'CategoryController@saveData'));
	Route::get('edit-category/{id}', array('as' => 'user-getEdit-article', 'uses' => 'CategoryController@edit'));
	Route::post('update-category', array('as' => 'user-update-category', 'uses' => 'CategoryController@updateCategory'));
	Route::post('delete-category', array('as' => 'user-delete-category', 'uses' => 'CategoryController@deleteCategory'));
});
