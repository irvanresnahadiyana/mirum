<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => 'sentinel_auth'], function () {
	Route::group(['middleware' => 'admin_auth'], function () {
		Route::get('dashboard', array('as' => 'admin-dashboard', 'uses' => 'DashboardController@index'));
		Route::get('profile', array('as' => 'admin-profile', 'uses' => 'ProfileController@index'));
		Route::post('update-profile', array('as' => 'admin-update-profile', 'uses' => 'ProfileController@updateProfile'));
		Route::get('update-password', array('as' => 'admin-update-password', 'uses' => 'ProfileController@updatePassword'));
		Route::post('save-password', array('as' => 'admin-save-password', 'uses' => 'ProfileController@updatePasswordSave'));

		/* Article */
		Route::get('article', array('as' => 'admin-article', 'uses' => 'ArticleController@index'));
		Route::get('datatableArticle', array('as' => 'admin-article-datatable', 'uses' => 'ArticleController@getDataTable'));
		Route::get('create-article', array('as' => 'admin-create-article', 'uses' => 'ArticleController@create'));
		Route::post('save-article', array('as' => 'admin-save-article', 'uses' => 'ArticleController@saveData'));
		Route::get('view-article/{slug}', array('as' => 'admin-view-article', 'uses' => 'ArticleController@view'));
		Route::post('edit', array('as' => 'admin-edit-article', 'uses' => 'ArticleController@edit'));
		Route::get('edit-article/{slug}', array('as' => 'admin-getEdit-article', 'uses' => 'ArticleController@edit'));
		Route::post('update-article', array('as' => 'admin-update-article', 'uses' => 'ArticleController@updateArticle'));
		Route::post('delete-article', array('as' => 'admin-delete-article', 'uses' => 'ArticleController@deleteArticle'));

		/* Category */
		Route::get('category', array('as' => 'admin-category', 'uses' => 'CategoryController@index'));
		Route::get('datatableCategory', array('as' => 'admin-category-datatable', 'uses' => 'CategoryController@getDataTable'));
		Route::get('create-category', array('as' => 'admin-create-category', 'uses' => 'CategoryController@create'));
		Route::post('save-category', array('as' => 'admin-save-category', 'uses' => 'CategoryController@saveData'));
		Route::get('edit-category/{id}', array('as' => 'admin-getEdit-article', 'uses' => 'CategoryController@edit'));
		Route::post('update-category', array('as' => 'admin-update-category', 'uses' => 'CategoryController@updateCategory'));
		Route::post('delete-category', array('as' => 'admin-delete-category', 'uses' => 'CategoryController@deleteCategory'));
	});
});
