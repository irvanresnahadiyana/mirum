<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('activations')->truncate();
        DB::table('persistences')->truncate();
        DB::table('reminders')->truncate();
        DB::table('roles')->truncate();
        DB::table('role_users')->truncate();
        DB::table('throttle')->truncate();

    	  $default_password = '12345678';


        // ROLE
        $admin = [
    			'name' => 'Administrator',
    			'slug' => 'administrator',
    		];
    		$adminRole = Sentinel::getRoleRepository()->createModel()->fill($admin)->save();

        $user = [
    			'name' => 'User',
    			'slug' => 'user',
    		];
    		$userRole = Sentinel::getRoleRepository()->createModel()->fill($user)->save();


        // ROLE

    		$admin = [
    			'email'    => 'admin@mirum.com',
    			'password' => $default_password,
                'first_name' => 'Super',
                'last_name' => 'Admin',
    		];


    		$users = [
    			[
    				'email'    => 'user1@mirum.com',
    				'password' => $default_password,
            'first_name' => 'User',
            'last_name' => 'Demo1',
    			],
    			[
    				'email'    => 'user2@mirum.com',
    				'password' => $default_password,
            'first_name' => 'User',
            'last_name' => 'Demo3',
    			],
    			[
    				'email'    => 'user3@mirum.com',
    				'password' => $default_password,
            'first_name' => 'User',
            'last_name' => 'Demo3',
    			],
    		];


        // REGISTER

    		$adminUser = Sentinel::registerAndActivate($admin);

        $roleAdmin = Sentinel::findRoleByName('Administrator');
        $roleAdmin->users()->attach($adminUser);

    		foreach ($users as $user)
    		{
    			$userRegister = Sentinel::registerAndActivate($user);
          $roleUser = Sentinel::findRoleByName('User');
          $roleUser->users()->attach($userRegister);
    		}
    }
}
