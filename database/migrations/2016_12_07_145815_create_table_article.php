<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
      Schema::create('articles', function($table) {
               $default = '';
               $table->bigIncrements('id');
               $table->integer('category_id')->default(0);
               $table->string('title');
               $table->string('slug');
               $table->text('short_description')->nullable();
               $table->text('content')->nullable();
               $table->string('image')->nullable()->default($default);
               $table->string('thumbnail')->nullable()->default($default);
               $table->timestamps();
           });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('articles');
    }
}
